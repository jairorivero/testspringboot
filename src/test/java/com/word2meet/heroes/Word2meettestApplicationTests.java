package com.word2meet.heroes;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.MimeTypeUtils;

import com.word2meet.heroes.controller.HeroeController;
import com.word2meet.heroes.dao.HeroeRepository;
import com.word2meet.heroes.entity.Heroe;
import com.word2meet.heroes.service.HeroeService;

@WebMvcTest(HeroeController.class)
class Word2meettestApplicationTests {
	
	@MockBean
	HeroeRepository heroeRepository;

	@MockBean
	private HeroeService heroeService;
	
	@Autowired
	private MockMvc mock;
	
	
	@Test
	void findAllTest() throws Exception {
		
		List<Heroe> items = new ArrayList<>();
		items.add(new Heroe(1,"Superman","Vuela"));
		items.add(new Heroe(2,"Aquaman","agua"));
		Mockito.when(heroeService.findAll()).thenReturn(items);
		
		this.mock.perform(get("/heroe").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MimeTypeUtils.APPLICATION_JSON_VALUE))
			.andExpect(content().json("[{\"id\": 1,\"name\": \"Superman\",\"power\": \"Vuela\"}, {\"id\": 2,\"name\": \"Aquaman\",\"power\": \"agua\"}]"));
		
	}
	

}
