package com.word2meet.heroes.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.word2meet.heroes.entity.Heroe;
import com.word2meet.heroes.service.HeroeService;

@RestController
@ResponseBody
public class HeroeController {

	@Autowired
	HeroeService heroeService;
	
	@PostMapping("/heroe")
	public Heroe save(@RequestBody(required = true) Heroe heroe) throws Exception {
		return heroeService.save(heroe);
	}

	@PutMapping("/heroe")
	public Heroe update(@RequestBody(required = true) Heroe heroe) throws Exception {
		return heroeService.update(heroe);
	}

	@DeleteMapping("/heroe/{id}")
	public void delete(@PathVariable(required = true) Integer id) throws Exception {
		heroeService.delete(id);
	}

	@GetMapping("/heroe/{id}")
	public Heroe findById(@PathVariable(required = true) Integer id) throws Exception {
		return heroeService.findById(id);
	}

	@GetMapping("/heroe")
	public List<Heroe> findAll() throws Exception {
		return heroeService.findAll();
	}
	
	@GetMapping("/heroe/name/{name}")
	public List<Heroe> findAllByName(@PathVariable(required = true) String name) throws Exception {
		return heroeService.findAllByName(name);
	}
	
}
