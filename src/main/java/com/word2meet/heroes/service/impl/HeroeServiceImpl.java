package com.word2meet.heroes.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.word2meet.heroes.dao.HeroeDAO;
import com.word2meet.heroes.entity.Heroe;
import com.word2meet.heroes.exceptions.HeroeNotFoundException;
import com.word2meet.heroes.exceptions.HeroeNotSaveException;
import com.word2meet.heroes.service.HeroeService;

@Service
public class HeroeServiceImpl implements HeroeService {

	@Autowired
	HeroeDAO heroeDAO;
	
	@Override
	public Heroe save(Heroe heroe) throws HeroeNotSaveException {
		return heroeDAO.save(heroe);
	}

	@Override
	public Heroe update(Heroe heroe) throws HeroeNotSaveException, HeroeNotFoundException {
		return heroeDAO.update(heroe);
	}

	@Override
	public void delete(Integer id) throws HeroeNotFoundException {
		heroeDAO.delete(id);
	}

	@Override
	public List<Heroe> findAll() {
		return heroeDAO.findAll();
	}

	@Override
	public Heroe findById(Integer id) throws HeroeNotFoundException  {
		return heroeDAO.findById(id);
	}

	@Override
	public List<Heroe> findAllByName(String name) {
		return heroeDAO.findAllByName(name);
	}

}
