package com.word2meet.heroes.service;

import java.util.List;

import com.word2meet.heroes.entity.Heroe;
import com.word2meet.heroes.exceptions.HeroeNotFoundException;
import com.word2meet.heroes.exceptions.HeroeNotSaveException;

public interface HeroeService {
	
	public Heroe save(Heroe heroe) throws HeroeNotSaveException;

	public Heroe update(Heroe heroe) throws HeroeNotSaveException, HeroeNotFoundException;
	
	public void delete(Integer id) throws HeroeNotFoundException;

	public List<Heroe> findAll();

	public Heroe findById(Integer id) throws HeroeNotFoundException ;

	public List<Heroe> findAllByName(String name);
	
}
