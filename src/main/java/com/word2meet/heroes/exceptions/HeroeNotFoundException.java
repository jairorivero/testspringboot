package com.word2meet.heroes.exceptions;

public class HeroeNotFoundException extends Exception {
	
	private static final long serialVersionUID = 1L;
	 
	public HeroeNotFoundException() {
		super("Heroe no existe!");
	}

}
