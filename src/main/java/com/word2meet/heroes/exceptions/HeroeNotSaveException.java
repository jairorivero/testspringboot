package com.word2meet.heroes.exceptions;

public class HeroeNotSaveException extends Exception {
	
	private static final long serialVersionUID = 1L;
	 
	public HeroeNotSaveException() {
		super("Heroe no puede ser registrado!");
	}

}
