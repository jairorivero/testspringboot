package com.word2meet.heroes.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.word2meet.heroes.entity.Heroe;

@Repository
public interface HeroeRepository extends JpaRepository<Heroe, Integer> {
	
	public Optional<Heroe> findByName(String name);

}
