package com.word2meet.heroes.dao.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.word2meet.heroes.dao.HeroeDAO;
import com.word2meet.heroes.dao.HeroeRepository;
import com.word2meet.heroes.entity.Heroe;
import com.word2meet.heroes.exceptions.HeroeNotFoundException;
import com.word2meet.heroes.exceptions.HeroeNotSaveException;

@Service
public class HeroeDAOImpl implements HeroeDAO {

	@Autowired
	HeroeRepository heroeRepository;

	@Override
	public Heroe save(Heroe heroe) throws HeroeNotSaveException {
		try {
			return heroeRepository.save(heroe);
		} catch(Exception e) {
			throw new HeroeNotSaveException();
		}
	}

	@Override
	public Heroe update(Heroe heroe) throws HeroeNotSaveException, HeroeNotFoundException {
		Heroe heroeOld = heroeRepository.findById(heroe.getId()).orElse(null);
		if(heroeOld==null) {
			throw new HeroeNotFoundException();
		}
		try {
			return heroeRepository.save(heroe);
		} catch(Exception e) {
			throw new HeroeNotSaveException();
		}
		
	}

	@Override
	public void delete(Integer id) throws HeroeNotFoundException {
		Heroe heroe = heroeRepository.findById(id).orElse(null);
		if (heroe == null) {
			throw new HeroeNotFoundException();
		}
		heroeRepository.delete(heroe);
		;
	}

	@Override
	public List<Heroe> findAll() {
		return heroeRepository.findAll();
	}

	@Override
	public Heroe findById(Integer id) throws HeroeNotFoundException {
		Heroe heroe = heroeRepository.findById(id).orElse(null);
		if(heroe==null) {
			throw new HeroeNotFoundException();
		}
		return heroe;
	}

	@Override
	public List<Heroe> findAllByName(String name) {
		return heroeRepository
				.findAll()
				.stream()
				.filter( (item) -> (item.getName().toLowerCase().indexOf(name.toLowerCase())!=-1) )
				.collect(Collectors.toList());
	}

}
