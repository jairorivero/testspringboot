package com.word2meet.heroes;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.word2meet.heroes.dao.HeroeRepository;
import com.word2meet.heroes.entity.Heroe;

@SpringBootApplication
public class Word2meettestApplication {

	@Autowired
	private HeroeRepository heroeRepository;

	public static void main(String[] args) {
		SpringApplication.run(Word2meettestApplication.class, args);
	}

	@Bean
	InitializingBean sendDatabase() {
		return () -> {
			heroeRepository.save(new Heroe("Superman", "Vuela"));
			heroeRepository.save(new Heroe("Hulk", "Super fuerte"));
			heroeRepository.save(new Heroe("Batman", "Millonario inteligente"));
			heroeRepository.save(new Heroe("Mujer Maravilla", "Belleza y fuerza"));
			heroeRepository.save(new Heroe("Linterna verde", "anillo magico"));
			heroeRepository.save(new Heroe("Thor", "Dios del rayo"));
		};
	}

}
