package com.word2meet.heroes.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Heroe {

	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(unique=true)
	private String name;
	@Column
	private String power;
	
	public Heroe() {
		
	}
	public Heroe(Integer id, String name, String power) {
		this.id = id;
		this.name = name;
		this.power = power;
	}
	public Heroe(String name, String power) {
		this.name = name;
		this.power = power;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPower() {
		return power;
	}
	public void setPower(String power) {
		this.power = power;
	}
	
	
}
